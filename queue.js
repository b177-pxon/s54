let collection = [];

// Write the queue functions below.


let print = () => {
	return collection;
}

let enqueue = (name) => {
	let i = collection.length;
	collection[i] = name;
	return collection;
}

let dequeue = () => {
	for(let i = 0; i < (collection.length-1); i++ ){
		collection[i] = collection[i+1];
	}
	collection.length -= 1; 
	return collection;
}

let front = () => {
	return collection[0];
}

let size = () => {
	return collection.length;
}

let isEmpty = () => {
	if(collection.length === 0){
		return true;
	}
	else{
		return false;
	} 
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};